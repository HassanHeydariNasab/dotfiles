(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(android-mode-sdk-dir "~/adt-bundle-linux-x86-20140702/sdk")
 '(column-number-mode t)
 '(company-backends
   (quote
    (company-c-headers company-bbdb company-nxml company-css company-eclim company-clang company-xcode company-cmake company-capf company-files
                       (company-dabbrev-code company-gtags company-etags company-keywords)
                       company-oddmuse company-dabbrev)))
 '(delete-selection-mode t)
 '(elpy-syntax-check-command "flake8 --ignore=E712")
 '(font-use-system-font t)
 '(global-company-mode t)
 '(inhibit-startup-buffer-menu nil)
 '(js3-auto-indent-p t)
 '(js3-boring-indentation t)
 '(js3-indent-level 4)
 '(js3-indent-on-enter-key t)
 '(package-selected-packages
   (quote
    (go-autocomplete flymake-go go-mode luarocks lua-mode folding json-mode importmagic jedi magit flycheck-pyflakes company-irony-c-headers company-irony auto-complete-clang-async clang-format auto-complete-clang auto-complete fic-mode web-server web-mode ## uuidgen tagedit sublimity pylint py-smart-operator py-autopep8 markdown-preview-eww markdown-edit-indirect html-check-frag handlebars-sgml-mode elpy duplicate-thing ac-js2 ac-html-csswatcher ac-html)))
 '(python-shell-interpreter "python3")
 '(safe-local-variable-values
   (quote
    ((company-clang-arguments "-I/usr/include/gtk-3.0/ -I/usr/include/at-spi2-atk/2.0 -I/usr/include/at-spi-2.0 -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I/usr/include/gtk-3.0 -I/usr/include/gio-unix-2.0/ -I/usr/include/mirclient -I/usr/include/mircore -I/usr/include/mircookie -I/usr/include/cairo -I/usr/include/pango-1.0 -I/usr/include/harfbuzz -I/usr/include/pango-1.0 -I/usr/include/atk-1.0 -I/usr/include/cairo -I/usr/include/pixman-1 -I/usr/include/freetype2 -I/usr/include/libpng16 -I/usr/include/gdk-pixbuf-2.0 -I/usr/include/libpng16 -I/usr/include/glib-2.0 -I/usr/lib/x86_64-linux-gnu/glib-2.0/include"))))
 '(save-place-mode t nil (saveplace))
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(tool-bar-position (quote right)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "black" :foreground "white" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 114 :width normal :foundry "PfEd" :family "Vazir Code"))))
 '(flymake-errline ((t nil))))
 
 (require 'ido)
 (ido-mode t)

 (package-initialize)
 (elpy-enable)

(global-linum-mode)
(global-hl-line-mode +1)

;;android MODE
;; Omit the next line if installed through ELPA
(add-to-list 'load-path "~/android-mode")
(require 'android-mode)


(add-to-list 'auto-mode-alist '("\\.js\\'" . js3-mode))
;;(add-to-list 'package-archives
;;	     '("melpa" . "http://melpa.milkbox.net/packages/") t)

(require 'package) ;; You might already have this line
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (url (concat (if no-ssl "http" "https") "://melpa.org/packages/")))
  (add-to-list 'package-archives (cons "melpa" url) t))
(when (< emacs-major-version 24)
  ;; For important compatibility libraries like cl-lib
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize) ;; You might already have this line

(setq compilation-read-command nil)
(global-set-key "\C-x\C-m" 'compile)

(add-hook 'html-mode-hook (lambda () (html-check-frag-mode 1)))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css?\\'" . web-mode))
(setq web-mode-ac-sources-alist
  '(("css" . (ac-source-css-property))
    ("html" . (ac-source-words-in-buffer ac-source-abbrev))))
(setq-default indent-tabs-mode nil)
;;(ac-config-default)

(require 'duplicate-thing)
(global-set-key (kbd "M-p") 'duplicate-thing)

(defun execute-c-program ()
  (interactive)
  (save-buffer)
  (defvar foo)
  ;;(setq foo (concat "gcc -o g " (buffer-name) " /usr/lib64/libXbgi.a -lX11 -lm -fPIC && ./g" ))
  (setq foo (concat "gcc -O0 -g -I/usr/X11R6/include -L/usr/X11R6/lib -o g " (buffer-name) " /usr/lib/libXbgi.a -lX11 -lm && ./g" ))
  
  (shell-command foo))

(global-set-key [C-f9] 'execute-c-program)

(add-hook 'after-init-hook 'global-company-mode)

;;(require 'company)
(require 'company-irony-c-headers)
;;(add-hook 'after-init-hook 'global-company-mode)
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-irony 'company-c-headers 'company-irony-c-headers)
  )

(require 'flycheck)
(eval-after-load 'flycheck
  '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
;;(setq company-backends (delete 'company-semantic company-backends))


(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(add-hook 'objc-mode-hook 'irony-mode)

(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

;; (eval-after-load 'company
;;   '(progn
;;      (define-key c-mode-map [(tab)] 'company-complete)))
;;(global-set-key "\t" 'company-complete-common)
(global-set-key [(tab)] 'company-complete-common)

(add-to-list 'auto-mode-alist '("\\.ui?\\'" . xml-mode))

;;(define-key c-mode-map  [(tab)] 'company-complete)
;;(define-key c++-mode-map  [(tab)] 'company-complete)
;;(semantic-add-system-include "/usr/include/gtk-3.0/")

;;(add-to-list 'company-c-headers-path-system "/usr/include/c++/4.8/")
;;(add-to-list 'company-c-headers-path-system "/usr/include/gtk-3.0/")

(require 'flycheck-pyflakes)
;;(add-hook 'python-mode-hook 'flycheck-mode)

(add-to-list 'auto-mode-alist '("\\.jsx?\\'" . js2-jsx-mode))
(add-to-list 'interpreter-mode-alist '("node" . js2-jsx-mode))

(add-to-list 'load-path "~/yafolding")

(defvar yafolding-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<C-S-return>") #'yafolding-hide-parent-element)
    (define-key map (kbd "<C-M-return>") #'yafolding-toggle-all)
    (define-key map (kbd "<C-return>") #'yafolding-toggle-element)
    map))

(global-set-key (kbd "s-d y") 'yafolding-discover)

(require 'yafolding)
(define-key yafolding-mode-map (kbd "<C-S-return>") nil)
(define-key yafolding-mode-map (kbd "<C-M-return>") nil)
(define-key yafolding-mode-map (kbd "<C-return>") nil)
(define-key yafolding-mode-map (kbd "C-c <C-M-return>") 'yafolding-toggle-all)
(define-key yafolding-mode-map (kbd "C-c <C-S-return>") 'yafolding-hide-parent-element)
(define-key yafolding-mode-map (kbd "C-c <C-return>") 'yafolding-toggle-element)

(add-hook 'python-mode 'yafolding-mode)

(add-to-list 'load-path "~/.emacs.d/lua-mode")

(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

(require 'go-autocomplete)
(require 'auto-complete-config)
(define-key ac-mode-map (kbd "M-TAB") 'auto-complete)

(eval-after-load "go-mode"
  '(require 'flymake-go))

